extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var mine_count = 40
var rng = RandomNumberGenerator.new()
var mine_positions = []
var player_name = ""

const GLITCH_CLICK_COUNT = 6
const GLITCHED_CELLS_COUNT = 10

const GRID_ADJUSTMENT = Vector2(10,0)
const GRID_MAX = Vector2(19,19)

# Called when the node enters the scene tree for the first time.
func _ready():
	new_game()

func set_mine_count(count):
	mine_count = count

func is_mine(x,y):
	return mine_positions.has(Vector2(x,y) - GRID_ADJUSTMENT)

func get_mine_coords():
	var coords = []
	for mine_pos in mine_positions:
		coords.append(mine_pos + GRID_ADJUSTMENT)
	return coords

func mines_around(x,y):
	x -= GRID_ADJUSTMENT.x
	y -= GRID_ADJUSTMENT.y
	var mines = 0
	for mp in mine_positions:
		if mp.x <= x+1 and mp.x >= x-1 and mp.y >= y-1 and mp.y <= y+1:
			mines += 1
	return mines

func new_game():
	rng.randomize()
	mine_positions = []
	for n in range(mine_count):
		var mine_added = false
		while !mine_added:
			var x = rng.randi_range(0, GRID_MAX.x)
			var y = rng.randi_range(0, GRID_MAX.y)
			if !mine_positions.has(Vector2(x,y)):
				mine_positions.append(Vector2(x,y))
				mine_added = true
	get_tree().change_scene("res://game.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
