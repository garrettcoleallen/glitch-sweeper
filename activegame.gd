extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var time_elapsed = 0
var game_over = false
var clicks_since_glitch = 0
var total_clicks = 0
var cell_click_history = []
var score_submitted = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_refresh_leaderboard()
	$Panel/SystemStabilityLbl/SystemStabilityValue.max_value = Game.GLITCH_CLICK_COUNT
	$Panel/SystemStabilityLbl/SystemStabilityValue.value = Game.GLITCH_CLICK_COUNT
	$Panel/Seed.text = str(Game.rng.seed)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !game_over:
		time_elapsed += delta
		$Panel/TimeElapsedLbl/TimeElapsedValue.text = str(stepify(time_elapsed, 0.001))
		$Panel/MinesLbl/MinesValue.text = str(Game.mine_count)

		
	$Panel/ClicksLbl/ClicksValue.text = str(total_clicks)

func _unhandled_input(event):
	if game_over:
		return
	if event is InputEventMouseButton:
		var mouse_button_event = event as InputEventMouseButton
		var tile_map = $TileMapActive
		var local_position = tile_map.to_local(mouse_button_event.global_position)
		var map_position = tile_map.world_to_map(local_position)
		var cell = tile_map.get_cell(map_position.x, map_position.y)
		
		
		if !(cell == 0 or cell == 12):
			return #not a valid cell to click
		
		# left click
		if mouse_button_event.button_index == 1 and !mouse_button_event.is_pressed():
			total_clicks += 1
			cell_click_history.append(map_position)
			if [0].has(cell): #if a valid clickable cell
				if Game.is_mine(map_position.x, map_position.y):
					for mine_pos in Game.get_mine_coords():
						tile_map.set_cell(mine_pos.x, mine_pos.y, 11)
					game_over = true
					$GameoverPanel.visible = true
					$GameoverPanel/LoseLabel.visible = true
					$GameoverPanel/WinLabel.visible = false
					_refresh_leaderboard()
				else:
					tile_map.set_cell(map_position.x, map_position.y, find_tile_for_clicked(map_position))
					auto_reveal(map_position, true)
					handle_glitch_counter_click()
					if check_for_win():
						game_over = true
						$GameoverPanel.visible = true
						$GameoverPanel/LoseLabel.visible = false
						$GameoverPanel/WinLabel.visible = true
						$GameoverPanel/WinLabel/SubmitYourScore.visible = true
						_refresh_leaderboard()
						
						
		#right click
		elif mouse_button_event.button_index == 2 and !mouse_button_event.is_pressed():
			if cell == 0:
				tile_map.set_cell(map_position.x, map_position.y, 12)
			elif cell == 12:
				tile_map.set_cell(map_position.x, map_position.y, 0)
			
			
func handle_glitch_counter_click():
	clicks_since_glitch += 1
	if clicks_since_glitch > Game.GLITCH_CLICK_COUNT:
		clicks_since_glitch = 0
		var glitchable_cells = []
		for cell in $TileMapActive.get_used_cells():
			if ![0,1,12].has($TileMapActive.get_cell(cell.x, cell.y)):
				glitchable_cells.append(cell)
		
		if glitchable_cells.size() < Game.GLITCHED_CELLS_COUNT:
			for cell in glitchable_cells:
				$TileMapActive.set_cell(cell.x, cell.y, 13)
		else:
			var glitch_cell_coords = []
			while glitch_cell_coords.size() < Game.GLITCHED_CELLS_COUNT:
				var try_coords = glitchable_cells[Game.rng.randi_range(0, glitchable_cells.size())-1]
				if !glitch_cell_coords.has(try_coords):
					glitch_cell_coords.append(try_coords)
			for gcell in glitch_cell_coords:
				$TileMapActive.set_cell(gcell.x ,gcell.y, 13)
	$Panel/SystemStabilityLbl/SystemStabilityValue.value = Game.GLITCH_CLICK_COUNT-clicks_since_glitch
	
	
	
var revealed = []
func auto_reveal(coords, first=false):
	if first:
		revealed = [coords]
	
	var mines_around = Game.mines_around(coords.x, coords.y)
	if  mines_around == 0:
		for x in range(-1,2):
			for y in range(-1,2):
				var next_coords = Vector2(x,y) + coords
				
				# Check in bounds and not repeating
				if x == 0 and y == 0:
					continue
#				if (next_coords.x > 19 or next_coords.x < 0 
#					or next_coords.y > 19 or next_coords.y < 0
				var next_coord_cell = $TileMapActive.get_cell(next_coords.x, next_coords.y)
				if ( ![0, 12].has(next_coord_cell)
					or revealed.has(next_coords)):
					continue
				
				var next_mines_around = Game.mines_around(next_coords.x, next_coords.y)
				revealed.append(next_coords)
				$TileMapActive.set_cell(next_coords.x, next_coords.y, find_tile_for_clicked(next_coords))
				if next_mines_around == 0:
					auto_reveal(next_coords)
	
	

func find_tile_for_clicked(tile_coords):
	var mines = Game.mines_around(tile_coords.x, tile_coords.y)
	return 1 + mines
	
func check_for_win():
	var tile_map = $TileMapActive
	for x in range(20):
		for y in range(20):
			if [0,12].has(tile_map.get_cell(x+Game.GRID_ADJUSTMENT.x,y+Game.GRID_ADJUSTMENT.y)):
				if !Game.is_mine(x+Game.GRID_ADJUSTMENT.x,y+Game.GRID_ADJUSTMENT.y):
					return false
				
	return true

func _on_NewGameButton_pressed():
	Game.new_game()


func _on_LBSubmitRequest_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	print(json)
	print(result)
	#print(json)
	#print(json)

func _refresh_leaderboard():
	$LBUpdateRequest.request("https://lb.userdefined.io/games/glitch-sweeper?asc=true&limit=10", PoolStringArray(), false, HTTPClient.METHOD_GET)

func _on_SubmitScoreBtn_pressed():
	if !score_submitted:
		$GameoverPanel/WinLabel/SubmitYourScore.visible = false
		score_submitted = true
		var request = JSON.print({ name = $GameoverPanel/WinLabel/SubmitYourScore/NameInput.text,
			score = time_elapsed,
			game = "glitch-sweeper",
			metaData = JSON.print({ 
				seed = Game.rng.seed,
				clicks = total_clicks,
				click_history = cell_click_history
			}) })
		var headers = PoolStringArray()
		headers.append("Content-Type: application/json")
		$LBSubmitRequest.request("https://lb.userdefined.io/games/submit", headers, false, HTTPClient.METHOD_POST, request)


func _on_LBUpdateRequest_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	var top10 = 1
	for item in json.result:
		if top10 > 10:
			break		
		get_node("GameoverPanel/Top10/Name/Name" + str(top10)).text = item.name
		get_node("GameoverPanel/Top10/Time/Time" + str(top10)).text = str(item.score)
		top10 += 1
